package br.com.desafio.conexa.service.impl;

import br.com.desafio.conexa.model.User;
import br.com.desafio.conexa.service.UserService;

public class UserServiceimpl implements UserService {

    public Integer validationTelephone(User user) {

        if (
                user.getTelephone().length() > 9 || user.getTelephone().length() < 8) {
            throw new RuntimeException("Número Inválido");
        }

        Integer telephoneStringForInteger = Integer.parseInt(user.getTelephone());
        return telephoneStringForInteger;
    }

}

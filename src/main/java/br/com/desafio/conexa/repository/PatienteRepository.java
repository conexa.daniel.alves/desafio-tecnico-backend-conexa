package br.com.desafio.conexa.repository;

import br.com.desafio.conexa.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatienteRepository extends JpaRepository<Patient,Integer> {
}

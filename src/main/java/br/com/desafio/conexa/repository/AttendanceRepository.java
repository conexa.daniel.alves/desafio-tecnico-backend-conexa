package br.com.desafio.conexa.repository;

import br.com.desafio.conexa.model.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttendanceRepository extends JpaRepository<Attendance,Integer> {

}

package br.com.desafio.conexa.controllers;

import br.com.desafio.conexa.model.Attendance;
import br.com.desafio.conexa.model.Patient;
import br.com.desafio.conexa.model.User;
import br.com.desafio.conexa.repository.AttendanceRepository;
import br.com.desafio.conexa.repository.PatienteRepository;
import br.com.desafio.conexa.repository.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class AttendanceController {

    private final AttendanceRepository attendanceRepository;

    private final UserRepository userRepository;

    private final PatienteRepository patienteRepository;

    public AttendanceController(AttendanceRepository attendanceRepository, UserRepository userRepository, PatienteRepository patienteRepository) {
        this.attendanceRepository = attendanceRepository;
        this.userRepository = userRepository;
        this.patienteRepository = patienteRepository;
    }

    @RequestMapping(method = RequestMethod.GET,path = "/todosAtendimentos")
    public ResponseEntity<List<Attendance>> listAllAttendance(){

        return ResponseEntity.ok(attendanceRepository.findAll());
    }

    @RequestMapping(method = RequestMethod.POST,path = "/attendance")
    @ResponseBody
    public ResponseEntity<Attendance> createAttendance(Attendance attendance, User healthProfessional, Patient patient){

        if(attendance.getScheduling().isBefore(LocalDateTime.now())){
            throw new RuntimeException("Realize um agendamento após o dia atual");
        }
        attendance.setPatient(patienteRepository.save(patient));

        return ResponseEntity.ok(attendanceRepository.save(attendance));
    }


}

package br.com.desafio.conexa.controllers;

import br.com.desafio.conexa.model.User;
import br.com.desafio.conexa.repository.UserRepository;
import br.com.desafio.conexa.service.impl.UserServiceimpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class UserController extends UserServiceimpl {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public UserController(UserRepository userRepository, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/listarTodos")
    public ResponseEntity<List<User>> listAll() {
        return ResponseEntity.ok(userRepository.findAll());
    }

    @RequestMapping(method = RequestMethod.POST, path = "/signup")
    @ResponseBody
    @Valid
    public ResponseEntity<User> create(User user) {

        if (!user.getPassword().equals(user.getConfirmPassword())) {
            throw new RuntimeException("Erro de Senha não identicas");
        }
        this.validationTelephone(user);

        user.setPassword(encoder.encode(user.getPassword()));
        return ResponseEntity.ok(userRepository.save(user));
    }

    public ResponseEntity<Boolean> ValidationPassword(@RequestParam String email,
                                                      @RequestParam String password) {

        boolean valid = false;
        Optional<User> optUser = userRepository.findByEmail(email);
        if (optUser.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(valid);
        }

        User userLogin = optUser.get();
        valid = encoder.matches(password, userLogin.getPassword());

        HttpStatus status = (valid) ? HttpStatus.OK : HttpStatus.UNAUTHORIZED;

        if (!valid) {
            return ResponseEntity.status(status).body(valid);
        }
        return null;
    }


    @RequestMapping(method = RequestMethod.GET, path = "/logout")
    public String logout(HttpServletRequest request,
                         HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response,
                    authentication);
        }

        return "redirect:/";
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> hadleValidationException(MethodArgumentNotValidException exception) {
        Map<String, String> erros = new HashMap<>();

        exception.getBindingResult().getAllErrors().forEach(error -> {
            String fildName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();

            erros.put(fildName, errorMessage);
        });
        return erros;
    }


}

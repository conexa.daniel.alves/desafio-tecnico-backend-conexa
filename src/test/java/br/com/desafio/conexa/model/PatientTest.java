package br.com.desafio.conexa.model;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PatientTest {

    @Test
    public void getPatient(){
        Patient patient = new Patient(
                1,
                "Paciente1",
                "13599177724");

        assertEquals("Paciente1",patient.getName());
        assertTrue(patient.toString().contains("Patient("));
    }

    @Test
    public void setPatient(){
        Patient patient = new Patient(
                1,
                "Paciente1",
                "13599177724");

        patient.setName("Daniel");
        assertTrue(patient.toString().contains("Daniel"));
    }

    @Test
    public void patientEmpty(){
        Patient patient = new Patient();
        assertFalse(patient.toString().isEmpty());
    }
}

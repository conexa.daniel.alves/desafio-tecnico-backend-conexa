package br.com.desafio.conexa.model;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UserTest {

    @Test
    public void getUserTest(){
        User user = new User(
                1,
                "daniel@teste.com",
                "123",
                "123",
                "Daniel",
                "medico",
                "13599177724",
                LocalDate.of(1994,5,11),
                "999999999");
        assertEquals("daniel@teste.com", user.getEmail());
        assertTrue(user.toString().contains("User("));
    }

    @Test
    public void setUser(){
        User user = new User(
                1,
                "daniel@teste.com",
                "123",
                "123",
                "Daniel",
                "medico",
                "13599177724",
                LocalDate.of(1994,5,11),
                "999999999");
        user.setPassword("12345");
        assertEquals("12345",user.getPassword());
        assertTrue(user.getPassword().contains("12345"));
    }

    @Test
    public void ValidationUserTest(){
        User user = new User(
                1,
                "daniel@teste.com",
                "123",
                "123",
                "Daniel",
                "medico",
                "1359917772555",
                LocalDate.of(1994,5,11),
                "999999999");

    }

    @Test
    public void UserEmpty(){
        User user = new User();
        assertFalse(user.toString().isEmpty());
    }


}

package br.com.desafio.conexa.model;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class AttendanceTest {

    @Test
    public void getAttendanceTest(){
        Patient patient = new Patient(1,
                "paciente1",
                "13599177724");

        Attendance attendance = new Attendance(
                1,
                LocalDateTime.of(
                        1994,
                        5,
                        11,
                        00,
                        00)
                ,patient);
        assertTrue(attendance.toString().contains("Attendance("));
    }
    @Test
    public void setAttendanceTest(){
        Patient patient = new Patient(1,
                "paciente1",
                "13599177724");

        Attendance attendance = new Attendance(
                1,
                LocalDateTime.of(
                        1994,
                        5,
                        11,
                        00,
                        00)
                ,patient);

        attendance.
                setScheduling(LocalDateTime.of(2022,
                        8,
                        20,
                        00,
                        00));
        assertTrue(attendance.toString().contains("Attendance("));
    }

    @Test
    public void AttendanceEmpty(){
        Attendance attendance = new Attendance();
        assertFalse(attendance.toString().isEmpty());
    }
}
